module tb_cases;
  reg arst_n;
  reg [3:0] inputs;
  integer i;

  cases DUT (
    .arst_n(arst_n),
    .a(inputs[0]),
    .b(inputs[1]),
    .c(inputs[2]),
    .d(inputs[3])
  );

  initial
  begin
    $dumpfile("simulation.vcd");
    $dumpvars(0, tb_cases);

    #2 arst_n = 1;
    #2 arst_n = 0;
    #2 arst_n = 1;

    #4 inputs = 0;
    for (i = 0; i < 3; i++)
      #4 inputs += 1;

    #2 arst_n = 0;
    #2 arst_n = 1;

    #4 inputs = 0;
    for (i = 0; i < 20; i++)
      #4 inputs += 1;

    #4 $stop;
  end
endmodule

module cases (
  input wire arst_n,
  input wire a,
  input wire b,
  input wire c,
  input wire d,
  output reg [1:0] o
);
  always @(arst_n, a, b, c, d)
  begin
    case ({a, b, c, d})
      4'b0000:
        o = 2'b00;
      4'b0001:
        o = 2'b01;
      4'b0010:
        o = 2'b01;
      4'b0011:
        o = 2'b00;
      4'b0100:
        o = 2'b01;
      4'b0101:
        o = 2'b10;
      4'b0110:
        o = 2'b10;
      4'b0111:
        o = 2'b10;
      4'b1000:
        o = 2'b01;
      4'b1001:
        o = 2'b00;
      4'b1010:
        o = 2'b00;
      4'b1011:
        o = 2'b00;
      4'b1100:
        o = 2'b10;
      4'b1101:
        o = 2'b10;
      4'b1110:
        o = 2'b00;
      4'b1111:
        o = 2'b10;
      default:
        o = 0;
    endcase

    if (!arst_n)
      o = 0;
  end
endmodule

module counter (
  input wire clk,
  input wire arst_n,
  output reg [7:0] q
);
  always @(posedge clk)
    if (arst_n)
      q <= q + 1;

  always @(arst_n)
    if (!arst_n)
      q <= 0;
endmodule

`timescale 1ns / 1ps

module tb_counter;
  reg arst_n;
  reg clk = 0;
  wire[7:0] q;

  initial begin
    $dumpfile("simulation.vcd");
    $dumpvars(0, tb_counter);
  end

  initial begin
    #5 arst_n = 0;
    #5 arst_n = 1;

    #3 arst_n = 0;
    #3 arst_n = 1;

    #19 arst_n = 0;
    #5 arst_n = 1;

    #100 $stop;
  end

  always #5 clk = !clk;

  counter DUT (
    .clk(clk),
    .arst_n(arst_n),
    .q(q)
  );

endmodule

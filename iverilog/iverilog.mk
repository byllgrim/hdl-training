SRC = $(MODULE).v tb_$(MODULE).v
VVP = tb_$(MODULE).vvp

$(VVP): $(SRC)
	iverilog -o $@ $(SRC)

simulation.vcd: $(VVP)
	vvp -n $(VVP) -vcd

run: simulation.vcd

show: simulation.vcd
	nohup gtkwave simulation.vcd &

clean:
	rm -rf *.vcd *.cf *.vvp

.PHONY: run

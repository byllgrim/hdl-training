# Synonyms for "ip core"

Ip core is a shitty name for the concept it represents.
I'm searching for alternative names.

Prefixes:
* chip-
* design-
* digital-
* gate-
* electronic-
* gateware-
* hardware-
* logic-
* reusable-
* vlsi-

Suffixes:
* -block
* -cell
* -component
* -controller
* -core
* -design
* -entity
* -library
* -model
* -module
* -processor
* -product
* -unit

Criteria:
1. The word suggests its meaning
  * Intellectual property applies to too many things to be an identifier for
    the particular concept of digital design modules
    * e.g. one doesn't say "ip music", "ip software", "ip autobiography"
2. Is short and memorable
  * "ip core", "app", and "crypto" are catchy words
3. Is substitutable in all occurances of "ip core"
  * "In electronic design an ip core is a reusable unit of logic, cell, or
    integrated circuit layout design"
  * "A strong reason to release an IP core, i.e. a bundle of files forming a
    gateware project, is to get help with the verification."

Proposals:
* Design block
* Gateware core
* Gateware project
* Virtual component
* Core
* Hardware block
* Electronic design

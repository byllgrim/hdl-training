import nmigen
from nmigen.back import verilog

class Counter(nmigen.Elaboratable):
    def elaborate(self, platform):
        m = nmigen.Module()

        count = nmigen.Signal(4)
        m.d.sync += count.eq(count + 1)

        return m

if __name__ == "__main__":
    top = Counter()
    v = verilog.convert(top)
    print(v)

import nmigen
from nmigen.back import verilog

class EmptyModule(nmigen.Elaboratable):
    def elaborate(self, platform):
        return nmigen.Module()

if __name__ == "__main__":
    print(verilog.convert(EmptyModule()))

// tb for counter

`default_nettype none

module tb;
  initial #200 $finish;

  initial begin
    $dumpfile("simulation.vcd");
    $dumpvars(0, tb);
  end

  reg clk = 0;
  initial forever #5 clk = ~clk;

  reg rst;
  initial begin
    rst = 0;
    #1 rst = 1;
    #10 rst = 0;
  end

  top top(.clk(clk), .rst(rst));
endmodule

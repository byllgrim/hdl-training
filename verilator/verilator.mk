SRC = $(MODULE).v $(MODULE).cpp
EXE = obj_dir/V$(MODULE)

$(EXE): $(SRC)
	verilator -Wall --cc --exe $(SRC)
	make -C obj_dir -f V$(MODULE).mk V$(MODULE)

run: $(EXE)
	./obj_dir/V$(MODULE)

clean:
	rm -rf obj_dir

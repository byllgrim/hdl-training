package Mypkg where

myhello :: Module Empty
myhello =
  module
    rules
      "myrule": when True ==> do
        $display "Hello, World!"
        $finish

package Top where

import Addcount

top :: Module Empty
top =
  module
    addcount_i <- addcount

    attempts :: Reg (Int 32) <- mkReg 0
    let operand = 5

    rules
      "poll_addcount": when True ==> do
        attempts := attempts + 1
        $display "addcount(%d) is %d" operand (addcount_i.getResult operand)

      "finish": when (attempts >= 10) ==> do
        $finish

module tb;

  reg clk;
  reg rst;

  top top(
    .CLK(clk),
    .RST_N(!rst)
    );

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  initial begin
    clk = 0;
    forever #5 clk = !clk;
  end

  initial begin
    rst = 0;
    #1;
    rst = 1;
    #20;
    rst = 0;
  end

endmodule

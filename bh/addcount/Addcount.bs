package Addcount where

interface Addcount_intf =
  getResult :: (Bit 24) -> ActionValue (Bit 24)

{-# verilog addcount #-}

addcount :: Module Addcount_intf
addcount =
  module
    count :: Reg (Bit 24) <- mkReg 0

    rules
      "count_up": when True ==> do
        count := count + 1

    interface Addcount_intf
      getResult x = return(count + x)

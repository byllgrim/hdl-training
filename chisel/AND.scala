import chisel3._
import chisel3.stage.ChiselStage

class AND extends Module {
  val io = IO(new Bundle {
    val a   = Input(UInt(16.W))
    val b   = Input(UInt(16.W))
    val out = Output(UInt(16.W))
  })

  io.out := io.a & io.b
}

object ANDDriver extends App {
  (new ChiselStage).emitVerilog(new AND)
}

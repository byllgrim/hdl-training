library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
  port (
    rst    : in  std_logic;
    a      : in  std_logic_vector(7 downto 0);  -- todo generic
    b      : in  std_logic_vector(7 downto 0);
    rcv_en : in  std_logic;
    y      : out std_logic_vector(7 downto 0);
    rdy    : out std_logic
    );
end entity adder;

architecture beh of adder is
  type async_state is (ready, receive, compute);

  signal state : async_state := receive;  -- todo deliberate wrong start state?
begin
  state_machine : process(rst, state, rcv_en, a, b)
    variable a_i : signed(7 downto 0);
    variable b_i : signed(7 downto 0);
  begin
    if rst = '1' then
      state <= ready;
      rdy   <= '0';
      y     <= (others => '0');
    else
      case state is
        when ready =>
          rdy <= '1';                     -- todo should be asked to start?
          if rcv_en = '1' then
            state <= receive;
          end if;
        when receive =>
          a_i := signed(a);
          b_i := signed(b);
          if rcv_en = '0' then
            state <= compute;
          end if;
        when compute =>
          y <= std_logic_vector(a_i + b_i);
      -- todo send to next receiver?
      end case;
    end if;
  end process state_machine;
end architecture beh;

library ieee;
use ieee.std_logic_1164.all;

entity tb_adder is
end entity tb_adder;

architecture tb of tb_adder is
  signal rst     : std_logic;
  signal a       : std_logic_vector(7 downto 0);
  signal b       : std_logic_vector(7 downto 0);
  signal rcv_en  : std_logic;
  constant tstep : time := 10 ns;
begin
  i_adder : entity work.adder
    port map(
      rst    => rst,
      a      => a,
      b      => b,
      rcv_en => rcv_en
      );

  stimulus : process
  begin
    wait for tstep;
    rst <= '1';
    wait for tstep;
    rst <= '0';
    wait for tstep;

    a      <= "10101010";
    b      <= "00001111";
    rcv_en <= '0';
    wait for tstep;
    rcv_en <= '1';
    wait for tstep;
    rcv_en <= '0';
    wait for tstep;

    wait;
  end process stimulus;
end architecture tb;

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity div3 is
  port (
    clk    : in  std_logic;
    arst_n : in  std_logic;
    q      : out std_logic
    );
end entity;

architecture rtl of div3 is
  signal cnt : unsigned(1 downto 0);
  signal q_i : std_logic;
begin
  q <= q_i;

  process(arst_n, clk)
  begin
    if (arst_n = '0') then
      q_i <= '0';
      cnt <= (others => '0');
    elsif (rising_edge(clk) or falling_edge(clk)) then
      cnt <= cnt + 1;
      if (cnt = 2) then
        q_i <= not q_i;
        cnt <= (others => '0');
      end if;
    end if;
  end process;
end;

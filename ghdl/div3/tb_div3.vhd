library ieee;

use ieee.std_logic_1164.all;

entity tb_div3 is
end;

architecture tb of tb_div3 is
  signal clk    : std_logic;
  signal arst_n : std_logic;
  signal tb_run : boolean;

  constant clk_per : time := 20 ns;
begin
  dut : entity work.div3
    port map (
      clk    => clk,
      arst_n => arst_n
      );

  clk <= not clk after clk_per / 2 when tb_run
         else '0';

  process
  begin
    tb_run <= true;
    arst_n <= '0';

    wait for 2 * clk_per;
    arst_n <= '1';
    wait for 16 * clk_per;

    tb_run <= false;
    wait;
  end process;
end;

SRC = $(MODULE).vhd tb_$(MODULE).vhd
LIB = work-obj93.cf

$(LIB): $(SRC)
	ghdl -a $(SRC)

run: $(LIB)
	ghdl -r tb_$(MODULE) --vcd=simulation.vcd

show: run
	gtkwave simulation.vcd

clean:
	rm -rf *.vcd *.cf

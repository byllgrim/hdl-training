library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
  generic (
    bits : positive
    );
  port (
    clk   : in  std_logic;
    rst_n : in  std_logic;
    q_out : out std_logic_vector(bits - 1 downto 0)
    );
end entity;

architecture rtl of counter is
  signal cnt : unsigned(q_out'length - 1 downto 0);  -- todo more elegant?
begin
  increment : process (rst_n, clk)
  begin
    if (rst_n = '0') then
      cnt <= (others => '0');
    elsif (rising_edge(clk)) then
      cnt <= cnt + 1;
    end if;
  end process;

  q_out <= std_logic_vector(cnt);
end architecture;

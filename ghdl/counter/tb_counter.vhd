library ieee;
use ieee.std_logic_1164.all;

entity tb_counter is
end entity;

-- todo vhdl style formatter program

-- todo no upper caps

architecture behavioral of tb_counter is
  constant bits    : positive := 3;     -- todo smart place to put this?
  constant clk_per : time     := 20 ns;

  signal clk_en : boolean;
  signal clk    : std_logic;
  signal q_out  : std_logic_vector(bits - 1 downto 0);  -- todo generic
  signal rst_n  : std_logic;
begin
  i_counter : entity work.counter
    generic map (
      bits => bits
      )
    port map (
      clk   => clk,
      rst_n => rst_n,
      q_out => q_out
      );

  clk <= not clk after clk_per / 2 when clk_en
         else '0';

  stimulus : process
  begin
    clk_en <= false;
    rst_n  <= '1';
    wait for 2 * clk_per;

    clk_en <= true;
    wait for 2 * clk_per;

    rst_n <= '0';
    wait for 2 * clk_per;

    rst_n <= '1';
    wait for 20 * clk_per;

    rst_n <= '0';
    wait for 5 * clk_per;

    rst_n <= '1';
    wait for 3 * clk_per;

    clk_en <= false;
    wait;
  end process;

-- todo asserts
end architecture;

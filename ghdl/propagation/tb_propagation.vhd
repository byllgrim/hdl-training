library ieee;

use ieee.std_logic_1164.all;

entity tb_propagation is
end entity;

architecture tb of tb_propagation is
  signal a : std_logic;
  signal b : std_logic;
begin
  dut : entity work.propagation
    port map (
      a => a,
      b => b
      );

  process
  begin
    a <= '0';
    b <= '0';
    wait for 2000 ps;

    a <= '1';
    wait for 2000 ps;

    b <= '1';
    wait for 4000 ps;

    b <= '0';
    wait for 2000 ps;

    a <= '0';
    wait for 5000 ps;

    wait;
  end process;
end architecture;

library ieee;

use ieee.std_logic_1164.all;

entity propagation is
  port (
    a : in  std_logic;
    b : in  std_logic;
    y : out std_logic
    );
end entity;

architecture nonsynth of propagation is
  signal gate_and : std_logic;

  signal gate_not   : std_logic;
  signal gate_not_d : std_logic;

  signal gate_xor   : std_logic;
  signal gate_xor_d : std_logic;
begin
  gate_xor_d <= gate_xor after 1 ns;
  gate_not_d <= gate_not after 4 ns;

  p_inputs : process(a, b)
  begin
    gate_xor <= a xor b;
    gate_not <= not b;
  end process;

  p_internal : process(gate_xor_d, gate_not_d)
  begin
    gate_and <= gate_xor_d and gate_not_d;
  end process;

  p_output : process(gate_and)
  begin
    y <= gate_and after 5 ns;
  -- todo this doesnt update as intended!
  end process;
end architecture;

module top;
  import uvm_pkg::*;
  import my_pkg::*;

  dut_if di();
  dut dut1(
    .di(di)
  );

  initial
  begin
    run_test("my_test");
  end
endmodule: top

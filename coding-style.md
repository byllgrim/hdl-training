VHDL Coding Style
=================
I fucking love coding styles and guidelines!

This document is appended as I learn.


Known TODOs
===========
Affixes for e.g. delayed or synchronized signals.

Architecture names: rtl, behavioral, etc.


Sources
=======
OpenCores Guidelines:
https://cdn.opencores.org/downloads/opencores_coding_guidelines.pdf

Kestrel Checklist:
http://chiselapp.com/user/kc5tja/repository/kestrel-3/wiki/Verilog%20Development%20Checklist

NandLand Guidelines:
https://www.nandland.com/articles/coding-style-recommendations-vhdl-verilog.html

Emacs VHDL Beautifier:
https://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/progmodes/vhdl-mode.el#n4481


Guidelines
==========

* http://chiselapp.com/user/kc5tja/repository/kestrel-3/wiki/Verilog%20Development%20Checklist
* https://github.com/NetFPGA/netfpga/wiki/VerilogCodingGuidelines
* https://people.ece.cornell.edu/land/courses/ece5760/Verilog/FreescaleVerilog.pdf
* https://www.doulos.com/knowhow/systemverilog/uvm/easier-uvm/easier-uvm-coding-guidelines/summary-of-the-easier-uvm-coding-guidelines
* https://www.nandland.com/articles/coding-style-recommendations-vhdl-verilog.html
* https://www.xilinx.com/support/documentation/white_papers/wp231.pdf
* http://www.pldworld.com/_hdl/2/_ref/coding_style/Verilog_Coding_Style_For_Efficient_Digital_Design.pdf
